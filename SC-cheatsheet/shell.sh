set -o xtrace # for debug
if [ $num -lt 10 -o $num -gt 100 ] # tests
then
    echo "Number $num is out of range"
elif [ ! -w $filename ]
then
    echo "Cannot write to $filename"
elif [ "mouse" = "cat" ]
then
    echo "shit"
fi
$0  #Name of this shell script itself.
$1  #Value of first command line parameter (similarly $2, $3, etc)
$#  #In a shell script, the number of command line parameters.
$*  #All of the command line parameters.
$-  #Options given to the shell.
$?  #Return the exit status of the last command.
$$  #Process id of script (really id of the shell running the script)
case "$1" in
    a) cmd1 ;;
    b) cmd2 ;;
    *) cmd4 ;;
esac
for number in 1 2 3
do
  echo -n $number
done
${!names[@]} # keys in an array
${names[$i]} # i-th value in an array
${} #Before a command is executed, any instances of ${variable} or $variable are replaced by the value of variable. 
$() #... $(command) or `command` are replaced by the output of command.
$[] #... $[expression] are replaced by the value of expression.
